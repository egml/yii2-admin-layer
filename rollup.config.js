// import path from 'path';
// import nodeResolvePlugin from 'rollup-plugin-node-resolve';
// import commonjsPlugin from 'rollup-plugin-commonjs';
// import jsonPlugin from 'rollup-plugin-json';
// import svgToSymbolPlugin from 'rollup-plugin-svg-to-symbol';
// import generateHtmlTemplatePlugin from 'rollup-plugin-generate-html-template';
// import sassPlugin from 'rollup-plugin-sass';
// import nodeSass from 'node-sass';
// import progressPlugin from 'rollup-plugin-progress';
// import { string as stringPlugin } from 'rollup-plugin-string';
// import svgoPlugin from 'rollup-plugin-svgo';
// import generateHtmlTemplatePlugin from 'rollup-plugin-generate-html-template'; // Это плагин возможно надо откатить до версии 1.1.0, если ругается `Cannot find module ...`
// import mustachePlugin from 'rollup-plugin-mustache';
// import notifyPlugin from 'rollup-plugin-notify';

// import Name from '@egml/utils/node/Name';
// import pkg from './package.json';

// var name = new Name(pkg[pkg.name].name, pkg[pkg.name].namespace);

// // ID модулей для исключения в конфигурации
// var moduleId = {
// 	config: path.resolve(__dirname, 'src/config.js'),
// 	modal: path.resolve(__dirname, 'src/modal.js'),
// };

// var svgToSymbolPluginInstance = svgToSymbolPlugin({
// 	extractId: function(file) {
// 		return name.css('svg_sprite-' + file.name);
// 	},
// 	svgo: {
// 		removeViewBox: false,
// 		removeDimensions: true,
// 		removeUselessStrokeAndFill: false,
// 		cleanupNumericValues: false,
// 		convertPathData: false,
// 		mergePaths: false,
// 	},
// });

// var jsonPluginInstance = jsonPlugin({
// 	compact: true,
// 	namedExports: false,
// });

export default {
	input: 'src/test.js',
	output: {
		file: 'test/main.js',
		format: 'iife',
		// name: name.dot(),
		// sourcemap: 'inline',
	},
	plugins: [
		// nodeResolvePlugin(),
		// commonjsPlugin(),
		// jsonPluginInstance,
		// svgToSymbolPluginInstance,
		// generateHtmlTemplatePlugin({
		// 	template: 'src/test.html',
		// 	target: 'index.html',
		// }),
		// mustachePlugin({
		// 	include: '**/*.mustache',
		// }),
		// sassPlugin({
		// 	insert: true,
		// }),
		// notifyPlugin(),
	],
	watch: {
		clearScreen: false,
	},
};

// export default [
	
// 	//	 ██████  ██    ██ ███████ ███████ ████████
// 	//	██       ██    ██ ██      ██         ██
// 	//	██   ███ ██    ██ █████   ███████    ██
// 	//	██    ██ ██    ██ ██           ██    ██
// 	//	 ██████   ██████  ███████ ███████    ██
// 	// 
// 	//	#guest
// 	{
// 		input: 'src/guest.js',
// 		output: {
// 			file: 'test/assets/' + name.kebab('guest.js'),
// 			format: 'iife',
// 			name: name.dot(),
// 			// sourcemap: 'inline',
// 		},
// 		plugins: [
// 			nodeResolvePlugin(),
// 			commonjsPlugin(),
// 			jsonPluginInstance,
// 			svgToSymbolPluginInstance,
// 			generateHtmlTemplatePlugin({
// 				template: 'src/test.html',
// 				target: 'index.html',
// 			}),
// 		],
// 	},

// 	//	 █████  ██    ██ ████████ ██   ██  ██████  ██████  ██ ███████ ███████ ██████
// 	//	██   ██ ██    ██    ██    ██   ██ ██    ██ ██   ██ ██    ███  ██      ██   ██
// 	//	███████ ██    ██    ██    ███████ ██    ██ ██████  ██   ███   █████   ██   ██
// 	//	██   ██ ██    ██    ██    ██   ██ ██    ██ ██   ██ ██  ███    ██      ██   ██
// 	//	██   ██  ██████     ██    ██   ██  ██████  ██   ██ ██ ███████ ███████ ██████
// 	// 
// 	//	#authorized
// 	{
// 		input: 'src/authorized.js',
// 		output: {
// 			file: 'test/assets/' + name.kebab('authorized.js'),
// 			format: 'iife',
// 			name: name.dot(),
// 			// sourcemap: 'inline',
// 		},
// 		plugins: [
// 			nodeResolvePlugin(),
// 			commonjsPlugin(),
// 			jsonPluginInstance,
// 			svgToSymbolPluginInstance,
// 		],
// 	},

// 	//	███    ███  ██████  ██████   █████  ██
// 	//	████  ████ ██    ██ ██   ██ ██   ██ ██
// 	//	██ ████ ██ ██    ██ ██   ██ ███████ ██
// 	//	██  ██  ██ ██    ██ ██   ██ ██   ██ ██
// 	//	██      ██  ██████  ██████  ██   ██ ███████
// 	// 
// 	//	#modal
// 	{
// 		input: 'src/modal.js',
// 		output: {
// 			file: 'test/assets/' + name.kebab('modal.js'),
// 			format: 'iife',
// 			name: name.dot('modal'),
// 			globals: {
// 				[ moduleId.config ]: name.dot('config'),
// 			},
// 			// sourcemap: 'inline',
// 		},
// 		external: [
// 			moduleId.config,
// 		],
//		external: function(id, parent, isResolved) {
//			if (!isResolved && !id.match(/^\./)) return true;
//		},
// 		plugins: [
// 			nodeResolvePlugin(),
// 			commonjsPlugin(),
// 			svgToSymbolPluginInstance,
// 		],
// 	},

// 	//	███    ███  ██████  ██████   █████  ██
// 	//	████  ████ ██    ██ ██   ██ ██   ██ ██
// 	//	██ ████ ██ ██    ██ ██   ██ ███████ ██
// 	//	██  ██  ██ ██    ██ ██   ██ ██   ██ ██
// 	//	██      ██  ██████  ██████  ██   ██ ███████
// 	// 
// 	//	██   ██ ████████ ███    ███ ██
// 	//	██   ██    ██    ████  ████ ██
// 	//	███████    ██    ██ ████ ██ ██
// 	//	██   ██    ██    ██  ██  ██ ██
// 	//	██   ██    ██    ██      ██ ███████
// 	// 
// 	//	#modal #html
// 	{
// 		input: 'src/modal-html.js',
// 		output: {
// 			file: 'test/assets/' + name.kebab('modal-html.js'),
// 			format: 'iife',
// 			globals: {
// 				[ moduleId.config ]: name.dot('config'),
// 			},
// 			// sourcemap: 'inline',
// 		},
// 		external: [
// 			moduleId.config,
// 		],
// 		plugins: [
// 			commonjsPlugin(),
// 			nodeResolvePlugin(),
// 		],
// 	},
	
// ];