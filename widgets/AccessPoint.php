<?php

namespace egml\adminLayer\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;
use yii\helpers\Html;

class AccessPoint extends Widget
{
	public $url;
	public $title;
	/**
	 * Настройка размера кнопки.
	 * Стили виджета проектировались с учетом размера шрифта родителя 16px. Если размер шрифта
	 * родительского элемента отличается, этот размер стоит передать в этом параметре,
	 * чтобы получить спроектированные визуальные размеры виджета.
	 */
	public $parentFontSize;
	
	public function init()
	{
		Yii::$app->get('view')->registerAssetBundle(MainAsset::className());

		if (!Yii::$app->adminLayer->accessGranted) {
			parent::init();
			
			$defaults = [
				'title' => 'Служебный вход',
			];
			foreach ($defaults as $key => $value) {
				if ($this->$key === null) {
					$this->$key = $value;
				}
			}
			if ($this->url === null) {
				$this->url = Yii::$app->adminLayer->adminHomeUrl;
			}
			if ($this->parentFontSize === null) {
				$this->parentFontSize = Yii::$app->params['bodyFontSize'] ? Yii::$app->params['bodyFontSize'] : 16;
			}
		}
	}
	
	public function run()
	{
		// Показывать иконку только неавторизованному пользователю, 
		// для авторизованного пользователя ничего не выводить
		if (!Yii::$app->adminLayer->accessGranted) {
			return $this->render('@egml/adminLayer/views/access-point', [
				'url' => Url::to($this->url),
				'title' => $this->title,
				'parentFontSize' => $this->parentFontSize,
			]);
		}
	}
}
