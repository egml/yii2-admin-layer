<?php

namespace egml\adminLayer\widgets;

use yii\web\View;
use yii\base\Widget;
use yii\helpers\Json;
use yii\helpers\Url;

class EditableArea extends Widget
{
	public $selector;
	public $url;
	public $title;
	
	// Подстройка позиции и размеров области относительно базовых значений
	public $offset;
	
	public function init()
	{
		parent::init();
		// Если виджет работает в местах, где `MainAsset` не подключен автоматически из `ApplicationComponent`
		Yii::$app->get('view')->registerAssetBundle(MainAsset::className());
	}
	
	public function run()
	{
		if (Yii::$app->adminLayer->accessGranted) {
			$options = [
				'selector' => $this->selector,
				'url' => Url::to($this->url),
				'title' => $this->title,
				'offset' => $this->offset,
			];
			$options = Json::encode($options);
			Yii::$app->get('view')->registerJs("adminLayer.editableArea.create($options);", View::POS_END);
		}
	}
}
