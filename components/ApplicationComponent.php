<?php

namespace egml\adminLayer\components;

use Yii;
use yii\base\Component;
use yii\base\Controller as BaseController;
use yii\web\Application;
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use Mustache_Engine;

class ApplicationComponent extends Component
{
	// Значения по умолчанию. Их можно задать, если определить 
	// этот компонент в конфиге приложения
	public $adminModuleId = 'admin';
	public $adminHomeUrl = [ 'admin/default/index' ];
	public $logoutUrl = [ 'site/logout' ];

	private $_accessGranted;

	public function getAccessGranted()
	{
		if ($this->_accessGranted == null) {
			$this->setAccessGranted();
		}
		return $this->_accessGranted;
	}

	public function setAccessGranted()
	{
		$this->_accessGranted = !Yii::$app->getUser()->getIsGuest();
	}
	
	public function init()
	{

		// Yii::$app->on(Application::EVENT_BEFORE_REQUEST, function() {
		// 	\app\components\VarDumper::die(Yii::$app->controller);
		// });

		Yii::$app->on(BaseController::EVENT_BEFORE_ACTION, function() {
			if ($this->isAdminModule(Yii::$app->controller->module)) {
				$this->setupInnerDocument();
			}
		});

		// $view = Yii::$app->get('view');

		// // Если не внутри модуля `$this->adminModuleId`, то подключить `MainAsset`
		// // Yii::$app->on(Application::EVENT_BEFORE_REQUEST, function() use ($view) {
		// // `EVENT_BEFORE_ACTION` выбрано, потому что это самое раннее событие, где определены текущие контроллер и модуль
		// Yii::$app->on(Application::EVENT_BEFORE_ACTION, function() use ($view) {
		// 	// \app\components\VarDumper::die(Yii::$app->controller);
		// 	// $view->on(View::EVENT_END_PAGE, function() use ($view) {
				
		// 		// // Рекурсивная функция проверки наличия модуля с ID `$this->adminModuleId` в иерархии любого другого модуля
		// 		// function isAdminModule($module) {
		// 		// 	if ($module->id == Yii::$app->adminLayer->adminModuleId) {
		// 		// 		return true;
		// 		// 	} else {
		// 		// 		if (isset($module->module)) {
		// 		// 			return isAdminModule($module->module);
		// 		// 		} else {
		// 		// 			return false;
		// 		// 		}
		// 		// 	}
		// 		// }
				
		// 		if (
		// 			// !isAdminModule(Yii::$app->controller->module) && // Не в модуле панели управления или ее подмодулях
		// 			!$this->isAdminModule(Yii::$app->controller->module) && // Не в модуле панели управления или ее подмодулях
		// 			// $this->accessGranted && // Есть доступ
		// 			// !isset($view->getAssetManager()->bundles[MainAsset::className()]) && // MainAsset не подключен
		// 			true // Чтобы быстро подключать/отключать условия
		// 		) {
		// 			// $view->registerJs('alert(\'asd\')', View::POS_END);
		// 			// $view->registerJsFile(Yii::getAlias('@web/test.js'), [
		// 			// 	'position' => View::POS_HEAD,
		// 			// 	'depends' => [
		// 			// 		'yii\\web\\JqueryAsset',
		// 			// 	],
		// 			// ]);


		// 			// $view->registerAssetBundle(MainAsset::className());


		// 			// // Поскольку на данном этапе бандлы уже не прикрепляются к странице, надо вызвать `registerAssetFiles` вручную (код взят из `yii\web\View::registerAssetFiles()`)
		// 			// function registerAssetFiles($name) {
		// 			// 	$view = Yii::$app->get('view');
		// 			// 	if (!isset($view->assetBundles[$name])) {
		// 		    //         return;
		// 		    //     }
		// 		    //     $bundle = $view->assetBundles[$name];
		// 		    //     if ($bundle) {
		// 		    //         foreach ($bundle->depends as $dep) {
		// 		    //             registerAssetFiles($dep);
		// 		    //         }
		// 		    //         $bundle->registerAssetFiles($view);
		// 		    //     }
		// 			// 	unset($view->assetBundles[$name]);
		// 			// }
		// 			// registerAssetFiles(MainAsset::className());
					
		// 			if ($this->accessGranted) {
		// 				$view->on(View::EVENT_END_BODY, function() use ($view) {
						
		// 					// HTML:
		// 					// -----
		// 					// HTML уже должен присутствовать в DOM во время загрузки JS
							
		// 					// - Модальное окно
		// 					$mustache = new Mustache_Engine;
		// 					$template = file_get_contents(Yii::getAlias('@egml/adminLayer/public-assets/src/modal.mustache'));
		// 					$parentFontSize = Yii::$app->params['bodyFontSize'] ? Yii::$app->params['bodyFontSize'] : 16;
		// 					$params = [
		// 						'scale' => 16 / $parentFontSize,
		// 					];
		// 					echo $mustache->render($template, $params);
							
		// 					// - Панель
		// 					echo $view->render('@egml/adminLayer/views/panel', [
		// 						'parentFontSize' => Yii::$app->params['bodyFontSize'] ? Yii::$app->params['bodyFontSize'] : 16,
		// 						'adminHomeUrl' => Url::to(Yii::$app->adminLayer->adminHomeUrl),
		// 					]);
		// 				});
		// 			}
		// 		}
		// 	// });
		// });
	}
	
	// Рекурсивная функция проверки наличия модуля с ID `$this->adminModuleId` в иерархии любого другого модуля
	public function isAdminModule($module)
	{
		if ($module->id == $this->adminModuleId) {
			return true;
		} else {
			if (isset($module->module)) {
				return self::isAdminModule($module->module);
			} else {
				return false;
			}
		}
	}

	public function setupInnerDocument()
	{
		$script = <<<SCRIPT
if (
	window != window.top && 
	typeof window.parent.egml == 'object' &&
	typeof window.parent.egml.adminLayer == 'object'
) {
	window.parent.egml.adminLayer.setupInnerDocument(document);
}
SCRIPT;
		Yii::$app->getView()->registerJs($script, View::POS_HEAD);
	}

	public function setModalWidth($width)
	{
		if (is_string($width)) {
			$width = "'$width'";
		}
		$script = <<<SCRIPT
if (
	window != window.top && 
	typeof window.parent.egml == 'object' &&
	typeof window.parent.egml.adminLayer == 'object'
) {
	window.parent.egml.adminLayer.modal.setWidth($width);
}
SCRIPT;
		Yii::$app->getView()->registerJs($script, View::POS_HEAD);
	}
}
