<?php

namespace egml\adminLayer\components;

// #TODO: Сохранение всех переходов внутри модального окна в истории браузера => предоставление URL для каждого перехода
// #TODO: API для добавления кнопок в панель быстрого доступа

use Yii;
use yii\base\Exception;
use yii\base\BootstrapInterface;
use yii\web\View;

class Bootstrap implements BootstrapInterface
{
	public function bootstrap($app)
	{
		// if (isset(Yii::$app->params['controlModule'])) {
		// 	$moduleId = Yii::$app->params['controlModule'];
		// } else {
		// 	$moduleId = 'control';
		// }
		
		// Control Overlay
		// NOTE: (Устаревшее) Bootstrap для ControlOverlay решено вообще убрать, т.к. в таком случае надо как-то фильтровать окружение, в котором запускается ControlOverlay, потому что он не нужен в других модулях, а только во фронтенде сайта, а такой фильтр реализовать нецелесообразно. Нужно инициализировать ControlOverlay через эссеты, которые подключаются только во фронтенде или там где нужно.
		// Yii::createObject(control_overlay\ControlOverlay::className());
		// $module = Yii::$app->getModule($moduleId);
		// if ($module) {
		// 	$module->set('overlay', control_overlay\ControlOverlayComponent::className());
		// } else {
		// 	throw new Exception("Модуль \"$moduleId\" не найден");
		// }
		// \app\components\VarDumper::die(Yii::$app->getRequest()->getQueryParams());
		// \app\components\VarDumper::die(Yii::$app->controller->getRoute());
		// На данном этапе нельзя определить, в каком модуле будет выполняться код
		// if (Yii::$app->has())
		// Yii::$app->set('controlOverlay', control_overlay\ApplicationComponent::className());
		// Yii::$app->get('controlOverlay');
		
		// Попытка №2:
		// Yii::$app->on(\yii\base\Application::EVENT_BEFORE_ACTION, function() {
		// 	if (Yii::$app->controller->module->module === null) { // Находимся в контроллере родительского приложения, а не модуля
		// 		if (!Yii::$app->has('controlOverlay')) {
		// 			Yii::$app->set('controlOverlay', control_overlay\ApplicationComponent::className());
		// 			// Yii::$app->get('controlOverlay');
		// 		}
		// 	}
		// });
		// Yii::$app->on(\yii\base\Application::EVENT_AFTER_ACTION, function() {
		// 	if (Yii::$app->controller->module->module === null) { // Находимся в контроллере родительского приложения, а не модуля
		// 		// Если есть доступ к панели управления и MainAsset еще не подключен (по зависимости от других эссетов или напрямую), то подключить его
		// 		if (
		// 			control_overlay\MainAsset::checkAccess() && // Есть доступ
		// 			!isset(Yii::$app->getAssetManager()->bundles[control_overlay\MainAsset::className()]) // MainAsset не загружен
		// 		) {
		// 			Yii::$app->getAssetManager()->register(control_overlay\MainAsset::className());
		// 		}
		// 	}
		// });
		
		// // Попытка №3:
		// // $className = control_overlay\ApplicationComponent::className();
		// if (!Yii::$app->has('controlOverlay')) {
		// // if (!Yii::$app->has('controlOverlay') || Yii::$app->get('controlOverlay')->class != $className) {
		// 	// Yii::$app->set('controlOverlay', $className);
		// 	Yii::$app->set('controlOverlay', control_overlay\ApplicationComponent::className());
		// }
		// // Инициализация компонента
		// Yii::$app->get('controlOverlay');
		
		if (!Yii::$app->has('adminLayer')) {
			Yii::$app->set('adminLayer', ApplicationComponent::class);
		}
		// Инициализация компонента
		Yii::$app->get('adminLayer');
	}
}
