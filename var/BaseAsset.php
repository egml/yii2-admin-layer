<?php
namespace egml\yii2Kit\control_overlay\components;

use yii\web\View;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

class BaseAsset extends \yii\web\AssetBundle
{
	public $params = [
		/**
		 * Настройка визуального CSS-масштаба.
		 * Стили проектировались с учетом размера шрифта родителя 16px. Если размер шрифта
		 * родительского элемента отличается, этот размер стоит передать в этом параметре 
		 * (см. `init()`), чтобы получить спроектированные визуальные размеры компонентов.
		 */
		'parentFontSize' => 16,
		'controlUrl' => null,
		'loggedIn' => false,
	];
	
	public function init() {
		// Инициализацию надо запускать, т.к. в ней выполняются важные операции с бандлом, 
		// например, `basePath` и `baseUrl` переводятся в реальные пути с помощью `\Yii::getAlias()`
		parent::init();
		
		// Установка CSS-масштаба
		if (isset(\Yii::$app->params['bodyFontSize'])) {
			$this->params['parentFontSize'] = \Yii::$app->params['bodyFontSize'];
		}
		
		// ...
		if (!isset(\Yii::$app->params['controlUrl'])) {
			$this->params['controlUrl'] = ['control/default/index'];
		} else {
			$this->params['controlUrl'] = \Yii::$app->params['controlUrl'];
		}
		
		// ...
		if (static::checkAccess()) {
			$this->params['loggedIn'] = true;
		}
	}

	public static function checkAccess()
	{
		return !\Yii::$app->getUser()->getIsGuest();
	}
}
