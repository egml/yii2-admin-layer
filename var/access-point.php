<?php
	use yii\helpers\Html;
	
	$tagOptions = [
		'title' => $this->title,
		'class' => [
			'egml-admin_layer-access_point',
		],
		'href' => Url::to($this->url),
		'style' => 'font-size:' . (16 / $this->parentFontSize) . 'em;',
	];
	if ($this->inline) {
		$tagOptions['class'][] = 'egml-admin_layer-access_point--inline';
	}
	
	echo Html::tag($this->tag, $this->content, $tagOptions);
?>

