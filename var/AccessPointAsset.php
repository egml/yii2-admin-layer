<?php
namespace egml\yii2Kit\control_overlay;

use Yii;
use yii\web\View;
use Mustache_Engine;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

class AccessPointAsset extends \yii\web\AssetBundle
{
	public $sourcePath = '@egml/yii2Kit/control_overlay/public/build';
	public $css = [
		'access_point.css',
	];
	public $js = [
		// 'js/access_point.js',
	];
	public $depends = [
		'egml\\yii2Kit\\control_overlay\\MainAsset',
		// 'yii\\web\\JqueryAsset',
	];
	public $publishOptions = [
		'forceCopy' => YII_ENV_DEV,
	];
	
	public function init()
	{
		parent::init();
		
		if (Yii::$app->controlOverlay->accessGranted) {
			$this->js[] = 'js/access_point.js';
		} else {
			$this->js[] = 'js/access_point_lazy.js';
		}
	}
	
	// public function registerAssetFiles($view)
	// {
	// 	parent::registerAssetFiles($view);
	// 	$am = $view->getAssetManager();
	// 	
	// 	// Выполнение `registerAssetFiles()` происходит как раз после триггера `EVENT_END_BODY` в `View::endBody()`, поэтому `on()` здесь использовать не нужно
	// 	// $view->on(View::EVENT_END_BODY, function($event) use ($view) {
	// 		// NOTE: require вызывает фатальную ошибку PHP. Пока не прикручена обработка фатальных ошибок, лучше использовать include.
	// 		// echo $event->sender->renderPhpFile($this->sourcePath . DS . 'img' . DS . 'sprites.svg');
	// 		// include $this->sourcePath . DS . 'img' . DS . 'sprite.svg';
	// 	// });
	// }
}
