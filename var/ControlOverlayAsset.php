<?php
namespace egml\yii2Kit\control_overlay;

use yii\web\View;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

class ControlOverlayAsset extends \yii\web\AssetBundle
{
	public $sourcePath = '@egml/yii2Kit/control_overlay/public/build';
	public $css = [
	];
	public $js = [
	];
	public $publishOptions = [
		'forceCopy' => YII_ENV_DEV,
	];
	public static $params = [
		/**
		 * Настройка визуального CSS-масштаба.
		 * Стили проектировались с учетом размера шрифта родителя 16px. Если размер шрифта
		 * родительского элемента отличается, этот размер стоит передать в этом параметре 
		 * (см. `init()`), чтобы получить спроектированные визуальные размеры компонентов.
		 */
		'parentFontSize' => 16,
		'controlUrl' => null,
		'loggedIn' => false,
	];
	
	public function init()
	{
		// Инициализацию надо запускать, т.к. в ней выполняются важные операции с бандлом, 
		// например, `basePath` и `baseUrl` переводятся в реальные пути с помощью `\Yii::getAlias()`
		parent::init();
		
		// // Установка CSS-масштаба
		// if (isset(\Yii::$app->params['bodyFontSize'])) {
		// 	$this->params['parentFontSize'] = \Yii::$app->params['bodyFontSize'];
		// }
		
		// ...
		if (!isset(\Yii::$app->params['controlUrl'])) {
			static::$params['controlUrl'] = ['control/default/index'];
		} else {
			static::$params['controlUrl'] = \Yii::$app->params['controlUrl'];
		}
		
		// ...
		if (static::checkAccess()) {
			static::$params['loggedIn'] = true;
		}
	}
	
//	public function registerAssetFiles($view)
//	{
//		parent::registerAssetFiles($view);
//		$am = $view->getAssetManager();
//		
//		// Регистрация SVG-спрайта
//		$url = $am->getAssetUrl($this, 'img/sprite.svg');
//		$script = <<<SCRIPT
//if (typeof DOMParser !== 'undefined') // IE6-
//$.ajax({
//	url: '$url',
//	type: 'get',
//	dataType: 'text',
//	success: function(data) {
//		// Для IE7-9, т.к. они не поддерживают встроенный парсинг свойства responseXML
//		// http://msdn.microsoft.com/en-us/library/ie/ms535874%28v=vs.85%29.aspx
//		var parser = new DOMParser();
//		var xml = parser.parseFromString(data, 'text/xml');
//		var svg = document.importNode(xml.documentElement, true);
//		document.body.appendChild(svg);
//	}
//});
//SCRIPT;
//		$view->registerJs($script, View::POS_END);
//	}
	
	public static function checkAccess()
	{
		return !\Yii::$app->getUser()->getIsGuest();
	}
}
