<?php
namespace egml\yii2Kit\control_overlay;

use Yii;
use yii\web\View;
use Mustache_Engine;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

class ModalAsset extends \yii\web\AssetBundle
{
	public $sourcePath = '@egml/yii2Kit/control_overlay/public/build';
	public $css = [
		'css/modal.css',
	];
	public $js = [
		'js/modal.chunk.js',
		'js/modal_component.js',
	];
	public $depends = [
		'egml\\yii2Kit\\control_overlay\\MainAsset',
		// 'yii\\web\\JqueryAsset',
	];
	public $publishOptions = [
		'forceCopy' => YII_ENV_DEV,
	];
	// public $params = [
	// 	/**
	// 	 * Настройка визуального CSS-масштаба.
	// 	 * Стили проектировались с учетом размера шрифта родителя 16px. Если размер шрифта
	// 	 * родительского элемента отличается, этот размер стоит передать в этом параметре 
	// 	 * (см. `init()`), чтобы получить спроектированные визуальные размеры компонентов.
	// 	 */
	// 	'parentFontSize' => 16,
	// 	'controlUrl' => null,
	// 	'loggedIn' => false,
	// ];
	
	public function init()
	{
		// Инициализацию надо запускать, т.к. в ней выполняются важные операции с бандлом, 
		// например, `basePath` и `baseUrl` переводятся в реальные пути с помощью `\Yii::getAlias()`
		parent::init();
		
		$view = \Yii::$app->getView();
		$view->on(View::EVENT_END_BODY, function($event) use ($view) {
			// Рендер mustache-шаблона модального окна
			$mustache = new Mustache_Engine;
			$template = file_get_contents(\Yii::getAlias('@egml/yii2Kit/control_overlay/public/src/modal.mustache'));
			$parentFontSize = Yii::$app->params['bodyFontSize'] ? Yii::$app->params['bodyFontSize'] : 16;
			$params = [
				'scale' => 16 / $parentFontSize,
			];
			echo $mustache->render($template, $params);
		});
		
		// // Установка CSS-масштаба
		// if (isset(\Yii::$app->params['bodyFontSize'])) {
		// 	$this->params['parentFontSize'] = \Yii::$app->params['bodyFontSize'];
		// }
		
//		// ...
//		if (isset(\Yii::$app->params['controlUrl'])) {
//			$this->params['controlUrl'] = Common::$controlUrl;
//		}
//		
//		// ...
//		if (Common::checkAccess()) {
//			$this->params['loggedIn'] = true;
//		}
	}
	
	public function registerAssetFiles($view)
	{
		parent::registerAssetFiles($view);
		
		// // Рендер mustache-шаблона модального окна
		// $mustache = new Mustache_Engine;
		// $template = file_get_contents(\Yii::getAlias('@egml/yii2Kit/control_overlay/public/src/modal.mustache'));
		// $parentFontSize = Yii::$app->params['bodyFontSize'] ? Yii::$app->params['bodyFontSize'] : 16;
		// $params = [
		// 	'scale' => 16 / $parentFontSize,
		// ];
		// echo $mustache->render($template, $params);
		
		/*
		// Регистрация SVG-спрайта
		$url = $am->getAssetUrl($this, 'img/sprite.svg');
		$script = <<<SCRIPT
if (typeof DOMParser !== 'undefined') // IE6-
$.ajax({
	url: '$url',
	type: 'get',
	dataType: 'text',
	success: function(data) {
		// Для IE7-9, т.к. они не поддерживают встроенный парсинг свойства responseXML
		// http://msdn.microsoft.com/en-us/library/ie/ms535874%28v=vs.85%29.aspx
		var parser = new DOMParser();
		var xml = parser.parseFromString(data, 'text/xml');
		var svg = document.importNode(xml.documentElement, true);
		document.body.appendChild(svg);
	}
});
SCRIPT;
		$view->registerJs($script, View::POS_END);
		*/
		
		// Выполнение `registerAssetFiles()` происходит как раз после триггера `EVENT_END_BODY` в `View::endBody()`, поэтому `on()` здесь использовать не нужно
		// $view->on(View::EVENT_END_BODY, function($event) use ($view) {
			// Регистрация HTML модального окна
			// echo $view->render('@egml/yii2Kit/control_overlay/views/modal', $this->params);
			// NOTE: require вызывает фатальную ошибку PHP. Пока не прикручена обработка фатальных ошибок, лучше использовать include.
			// echo $event->sender->renderPhpFile($this->sourcePath . DS . 'img' . DS . 'sprites.svg');
			// include $this->sourcePath . DS . 'img' . DS . 'sprite.svg';
		// });
	}
}
