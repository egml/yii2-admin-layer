<?php
namespace egml\yii2Kit\control_overlay;

use Yii;
use yii\web\View;

defined('DS') or define('DS', DIRECTORY_SEPARATOR);

class PanelAsset extends \yii\web\AssetBundle
{
	public $sourcePath = '@egml/yii2Kit/control_overlay/public/build';
	public $css = [
		// 'css/panel.css',
	];
	public $js = [
		// 'js/panel.js',
	];
	public $depends = [
		'egml\\yii2Kit\\control_overlay\\MainAsset',
		// 'yii\\web\\JqueryAsset',
	];
	public $publishOptions = [
		'forceCopy' => YII_ENV_DEV,
	];
	
	public function init()
	{
		// Инициализацию надо запускать, т.к. в ней выполняются важные операции с бандлом, 
		// например, `basePath` и `baseUrl` переводятся в реальные пути с помощью `\Yii::getAlias()`
		parent::init();
		
	}
	
	public function registerAssetFiles($view)
	{
		parent::registerAssetFiles($view);
		// Регистрация HTML панели
		echo $view->render('@egml/yii2Kit/control_overlay/views/panel', [
			'parentFontSize' => Yii::$app->params['bodyFontSize'] ? Yii::$app->params['bodyFontSize'] : 16,
			'controlHomeUrl' => \Yii::$app->controlOverlay->controlHomeUrl,
		]);
	}
	
}
