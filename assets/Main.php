<?php

namespace egml\adminLayer\assets;

use Yii;
use yii\web\Application;
use yii\web\View;
use yii\web\AssetBundle;
use yii\helpers\Json;
use yii\helpers\Url;
use Mustache_Engine;

class Main extends AssetBundle
{
	public $sourcePath = '@vendor/egml/yii2-admin-layer/public/build';
	// public $js = [
	// 	'egml-admin-layer-commons--authorized--guest.chunk.js',
	// ];
	public $jsOptions = [
		'position' => View::POS_HEAD,
	];
	// public $publishOptions = [
	// 	'forceCopy' => YII_ENV_DEV,
	// ];
	
	public function init()
	{
		parent::init();
		
		// Для гостя
		if (!Yii::$app->adminLayer->accessGranted) {
		
			// CSS/JS
			$this->css[] = 'egml-admin-layer-guest.css';
			$this->js[] = 'egml-admin-layer-guest.js';
			
		// Для авторизованного пользователя
		} else {
		
			// CSS/JS
			$this->css[] = 'egml-admin-layer-authorized.css';
			$this->css[] = 'egml-admin-layer-modal.css';
			$this->js[] = 'egml-admin-layer-authorized.js';
			$this->js[] = 'egml-admin-layer-modal.js';
		}

		Yii::$app->get('view')->on(View::EVENT_END_BODY, function() {

			$view = Yii::$app->get('view');
			$am = Yii::$app->get('assetManager');
			$parentFontSize = Yii::$app->params['bodyFontSize'] ? Yii::$app->params['bodyFontSize'] : 16;
			$publicPath = $this->baseUrl;

			// Передача зарегистрированных бандлов в JS (их базовых URL)
			// $bundles = [];
			// foreach ($am->bundles as $name => $bundle) {
			// 	$bundles[$name] = $bundle->baseUrl;
			// }
			// $passedData = [
			// 	'debug' => YII_DEBUG,
			// 	'name' => Yii::$app->name,
			// 	'baseUrl' => Url::to('@web'),
			// 	'absoluteBaseUrl' => Url::to('@web', true),
			// 	'params' => Yii::$app->params,
			// 	'assets' => $bundles,
			// ];
			// $passedData = Json::encode($passedData);

			$view->registerJs("\n".
				"\tegml.adminLayer.config.rootFontSize = $parentFontSize;\n".
				"\tegml.adminLayer.config.publicPath = '$publicPath/';\n"
			, View::POS_HEAD);

			// Регистрация HTML для авторизованного пользователя
			if (
				Yii::$app->adminLayer->accessGranted && // Пользователь авторизован
				!Yii::$app->adminLayer->isAdminModule(Yii::$app->controller->module) && // Не в модуле панели управления или ее подмодулях
				true
			) {
				// Модальное окно
				echo $view->render('@egml/adminLayer/views/modal', [
					'parentFontSize' => $parentFontSize,
				]);
				
				// Панель
				echo $view->render('@egml/adminLayer/views/panel', [
					'parentFontSize' => $parentFontSize,
					'adminHomeUrl' => Url::to(Yii::$app->adminLayer->adminHomeUrl),
					'logoutUrl' => Url::to(Yii::$app->adminLayer->logoutUrl),
				]);
			}
		});
	}
	
	// public function registerAssetFiles($view)
	// {
	// 	parent::registerAssetFiles($view);
		
	// 	$am = $view->getAssetManager();
	// 	// SVG-спрайт
	// 	// NOTE: Почему-то `getAssetUrl` не строит правильный путь, если вызвать напрямую в `init()`
	// 	$url = $am->getAssetUrl($this, 'img/egml-admin-layer-sprite.svg');
	// 	$view->registerJs("window.adminLayerCommonUtilities.importSVG('$url');", View::POS_BEGIN);
	// }
}
