<?php
	use yii\widgets\Spaceless;
?>
<?php Spaceless::begin() ?>
<div class="egml-admin_layer-modal" style="display:none; font-size:<?= 16 / $parentFontSize ?>em">
	<div class="egml-admin_layer-modal-clip">
		<div class="egml-admin_layer-modal-aligner">
			<div class="egml-admin_layer-modal-aligner-helper">
				<div class="egml-admin_layer-modal-box">
					<div class="egml-admin_layer-modal-content">
						<iframe class="egml-admin_layer-modal-content-iframe" src="" scrolling="no" aria-hidden="true"></iframe>
					</div>
					<button class="egml-admin_layer-modal-close" title="Закрыть">
						<svg class="egml-admin_layer-modal-close-icon" role="presentation" aria-hidden="true">
							<title>Закрыть диалог</title>
							<use xlink:href="#egml-admin_layer-svg_sprite-cross"></use>
						</svg>
					</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php Spaceless::end() ?>
