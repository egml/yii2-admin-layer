<?php
	use yii\widgets\Spaceless;
?>
<?php Spaceless::begin() ?>
<a class="egml-admin_layer-access_point" href="<?= $url ?>" title="<?= $title ?>" style="width:<?= 27 / $parentFontSize ?>em;">
	<span class="egml-admin_layer-access_point-scale_root" style="font-size:<?= 16 / $parentFontSize ?>em">
		<span class="egml-admin_layer-access_point-interact_area">
			<svg class="egml-admin_layer-access_point-icon" role="none" aria-hidden="true">
				<use xlink:href="#egml-admin_layer-svg_sprite-lock" />
			</svg>
		</span>
	</span>
</a>
<?php Spaceless::end() ?>
