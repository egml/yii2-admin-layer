<?php
	use yii\widgets\Spaceless;
	use yii\helpers\Url;
?>
<?php Spaceless::begin() ?>
<div class="egml-admin_layer-panel" style="font-size:<?= 16 / $parentFontSize ?>em">
	<a href="<?= $adminHomeUrl ?>" class="egml-admin_layer-panel-button" title="Открыть панель управления" onclick="egml.adminLayer.modal.show({ url: this.href, callback: egml.adminLayer.panel.hide })">
		<svg class="egml-admin_layer-panel-button-icon" role="none" aria-hidden="true">
			<use xlink:href="#egml-admin_layer-svg_sprite-cog_stroke" />
		</svg>
	</a>
	<a href="#" class="egml-admin_layer-panel-button" title="Показать редактируемые области" onclick="egml.adminLayer.editableArea.toggle()">
		<svg class="egml-admin_layer-panel-button-icon" role="none" aria-hidden="true">
			<use xlink:href="#egml-admin_layer-svg_sprite-editable_area_toggle" />
		</svg>
	</a>
	<a href="#" class="egml-admin_layer-panel-button" title="Выйти" onclick="egml.adminLayer.request({ url: '<?= $logoutUrl ?>', success: function() { if (this.status == 200) window.location.reload(true) } })">
		<svg class="egml-admin_layer-panel-button-icon" role="none" aria-hidden="true">
			<use xlink:href="#egml-admin_layer-svg_sprite-cross_shape" />
		</svg>
	</a>
</div>
<?php Spaceless::end() ?>
